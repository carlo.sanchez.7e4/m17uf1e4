using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemyCollisionController : MonoBehaviour
{
    public GameManager GM;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Debug.Log("Colisi� amb el Player");
            //Aplicar el dany
            GameObject pl = collision.gameObject;
            pl.GetComponent<DataPlayer>().lifes--;
            if (pl.GetComponent<DataPlayer>().lifes <= 0) {
                Destroy(pl);
                SceneManager.LoadScene("GameOver");
            }
        } else if (collision.CompareTag("Enemy"))
        {
            GM.increaseScore();
            Destroy(collision.gameObject);
        }
        Destroy(gameObject);
    }

    private void OnMouseDown()
    {
        GM.increaseScore();
        Destroy(gameObject);
    }
}
