using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject enemy;

    public Vector2 randomX;
    public Vector2 randomY;

    public float cooldown;

    // Start is called before the first frame update
    void Start()
    {
        Instantiate(enemy, new Vector3(Random.Range(randomX.x, randomY.y), Random.Range(randomY.x, randomX.y), 0), Quaternion.identity);
        cooldown = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown + 1 <= Time.time)
        {
            Instantiate(enemy, new Vector3(Random.Range(randomX.x, randomY.y), Random.Range(randomY.x, randomX.y), 0), Quaternion.identity);
            cooldown = Time.time;
        }
    }
}
