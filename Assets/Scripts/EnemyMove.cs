using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{
    private GameObject thisEnemy;
    private GameObject player;

    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        thisEnemy = this.gameObject;
        player = GameObject.Find("Player");

    }

    // Update is called once per frame
    void Update()
    {
        thisEnemy.transform.position = Vector3.MoveTowards(thisEnemy.transform.position, player.transform.position, movement());
    }

    private float movement()
    {
        return ((speed == 0f) ? 1f : speed) * Time.deltaTime;
    }
}
