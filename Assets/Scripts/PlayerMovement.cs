﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public DataPlayer DP;

    private float speed;

    public Transform female;
    public bool left;
    // Boolean que permet o no activar el poder de Gigantamax
    public bool gigantamax;
    public bool growing;
    public bool degrowing;
    public int growthCounter;

    // Variables de temps
    public float gigantamaxEnd;
    public float gigantamaxCooldown;


    // Start is called before the first frame update
    void Start()
    {
        DP = gameObject.GetComponent<DataPlayer>();
        speed = DP.Speed / DP.Weight;
        female = gameObject.transform;
        left = true;
        gigantamax = true;
        growing = false;
        degrowing = false;
        growthCounter = 0;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();

        Gigantamax();
    }

    private void Movement()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            PlayerMoveLeft();
        }
        else if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            PlayerMoveRight();
        }
        else if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
        {
            PlayerMoveUp();
        }
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            PlayerMoveDown();
        }
    }

    private void PlayerMoveLeft()
    {
        if (!left)
        {
            Flip();
            left = true;
        }
        female.position += Vector3.left * (speed * Time.deltaTime);
    }

    private void PlayerMoveRight()
    {
        if (left)
        {
            Flip();
            left = false;
        }
        female.position += Vector3.right * (speed * Time.deltaTime);
    }

    private void PlayerMoveUp()
    {
        female.position += Vector3.up * (speed * Time.deltaTime);
    }

    private void PlayerMoveDown()
    {
        female.position += Vector3.down * (speed * Time.deltaTime);
    }

    private void Gigantamax()
    {
        if (gigantamax)
        {
            if (Input.GetKey(KeyCode.Z))
            {
                growing = true;
                gigantamax = false;
            }
        }
        else if (growing)
        {
            if (growthCounter <= 5 && left)
            {
                female.localScale += new Vector3(1, 1, 0) * 1.1f;
                growthCounter++;
            }
            else if (growthCounter <= 5 && !left)
            {
                female.localScale += new Vector3(-1, 1, 0) * 1.1f;
                growthCounter++;
            }
            else
            {
                degrowing = true;
                gigantamaxEnd = Time.time;
                growing = false;
                growthCounter = 0;
            }
        }
        else if (degrowing)
        {
            if (gigantamaxEnd + 3 <= Time.time)
            {
                if (left)
                {
                    female.localScale = new Vector3(1, 1, 0);
                }
                else
                {
                    female.localScale = new Vector3(-1, 1, 0);
                }

                degrowing = false;
                gigantamaxCooldown = Time.time;
            }
        }
        else
        {
            if (gigantamaxCooldown + 5 <= Time.time)
            {
                gigantamax = true;
            }
        }
    }

    protected void Flip()
    {
        Vector3 theScale = female.localScale;
        theScale.x *= -1;
        female.localScale = theScale;
    }
}