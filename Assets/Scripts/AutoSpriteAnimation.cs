﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoSpriteAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private int animationSpritesIndex;

    public Patrol AM;

    //Save Sprite var to SpriteRenderer
    private SpriteRenderer spriteRenderer;

    // Start is called before the first frame update
    void Start()
    {
        AM = gameObject.GetComponent<Patrol>();
        animationSprites = gameObject.GetComponent<DataPlayer>().spriteArray;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = animationSprites[0];
        animationSpritesIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (AM.movement)
        {
            Animate();
        } else
        {
            spriteRenderer.sprite = animationSprites[1];
        }
    }

    public void Animate()
    {
        spriteRenderer.sprite = animationSprites[animationSpritesIndex];
        animationSpritesIndex++;

        if (animationSpritesIndex >= animationSprites.Length)
        {
            animationSpritesIndex = 0;
        }
    }
}
