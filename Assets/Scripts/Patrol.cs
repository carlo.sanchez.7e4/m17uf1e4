﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour
{
    public DataPlayer DP;
    public int count;

    public float distance;
    public float speed;

    public bool movement;
    public float waitTime;

    public Transform female;
    public bool left;
    

    // Start is called before the first frame update
    void Start()
    {
        DP = gameObject.GetComponent<DataPlayer>();
        speed = 4 / DP.Weight;
        distance = DP.Distance / speed;
        Debug.Log(distance);
        count = 0;
        female = gameObject.transform;
        left = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (movement)
        {
            Movement();
        } else
        {
            if (waitTime + 2 <= Time.time)
            {
                movement = true;
                Flip();
            }
        }
    }

    private void Movement()
    {
        if (left)
        {
            MoveLeft();
        }
        else
        {
            MoveRight();
        }
    }

    

    private void MoveLeft()
    {
        if (count <= distance)
        {
            female.position += Vector3.left * speed;
            count++;
        }
        else
        {
            left = false;
            count = 0;
            waitTime = Time.time;
            movement = false;
        }
    }

    private void MoveRight()
    {
        if (count <= distance)
        {
            female.position += Vector3.right * speed;
            count++;
        }
        else
        {
            left = true;
            count = 0;
            waitTime = Time.time;
            movement = false;
        }
    }

    protected void Flip()
    {
        Vector3 theScale = female.localScale;
        theScale.x *= -1;
        female.localScale = theScale;
    }
}
