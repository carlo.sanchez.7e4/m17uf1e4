﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Sorcerer,
    Knight,
    Assassin,
    Bard,
    Barbarian

}

public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    private string Name;
    public string Surname;
    public float Speed, Height, Weight, Distance;

    public int lifes;

    public PlayerKind Kind;

    //Les imatges están carregades desde el Unity Editor
    public Sprite[] spriteArray;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}