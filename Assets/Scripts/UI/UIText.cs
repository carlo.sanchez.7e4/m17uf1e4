﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIText : MonoBehaviour
{
    public itemList selector;
    private Text textOutput;
    private SpriteAnimation animData;
    private float framerate;

    public GameManager GM;

    // Start is called before the first frame update
    void Start()
    {
        textOutput = GetComponent<Text>();
        animData = FindObjectOfType<SpriteAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        framerate = 1 / Time.deltaTime;
        switch (selector)
        {
            case itemList.frames:
                textOutput.text = "Frames/s:\t" + framerate.ToString();
                break;
            case itemList.framerate:
                textOutput.text = "Framerate value:\t" + animData.framerate.ToString();
                break;
            case itemList.score:
                textOutput.text = "Score:\t" + GM.returnScore();
                break;
            case itemList.enemies:
                textOutput.text = "Enemies:\t" + GameObject.FindGameObjectsWithTag("Enemy").Length;
                break;
            default: 
                textOutput.text = "ERROR: "+selector+" no se ha encontrado como una función conocida.";
                break;

        }
    }
    public enum itemList
    {
        frames,
        framerate,
        score,
        enemies
    }
}
