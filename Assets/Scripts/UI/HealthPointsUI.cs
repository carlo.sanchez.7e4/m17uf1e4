using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPointsUI : MonoBehaviour
{
    public enum hearts
    {
        heart1,
        heart2,
        heart3
    }

    public hearts selector;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        GameObject pl = GameObject.Find("Player");
        switch (selector)
        {
            case hearts.heart3:
                if (pl.GetComponent<DataPlayer>().lifes < 3)
                {
                    gameObject.SetActive(false);
                }
                break;
            case hearts.heart2:
                if (pl.GetComponent<DataPlayer>().lifes < 2)
                {
                    gameObject.SetActive(false);
                }
                break;
        }
        
    }
}
