﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    private Sprite[] animationSprites;
    private int animationSpritesIndex;

    //Save Sprite var to SpriteRenderer
    private SpriteRenderer spriteRenderer;

    public float framerate;
    public float frames;

    private float cooldown;
    
    // Start is called before the first frame update
    void Start()
    {
        animationSprites = gameObject.GetComponent<DataPlayer>().spriteArray;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        spriteRenderer.sprite = animationSprites[0];
        animationSpritesIndex = 0;

        cooldown = Time.time;

        frames = 1 / Time.deltaTime;
        framerate = 1f / ((frames > 0) ? frames : 12);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
        {
            Animate();
        } else
        {
            spriteRenderer.sprite = animationSprites[1];
        }
    }

    public void Animate()
    {
        if (cooldown + 0.25f <= Time.time) {
            spriteRenderer.sprite = animationSprites[animationSpritesIndex];
            animationSpritesIndex++;
            cooldown = Time.time;
        }
        
        if (animationSpritesIndex >= animationSprites.Length)
        {
            animationSpritesIndex = 0;
        }
    }
}
